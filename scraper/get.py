import logging
import re
import ast
import sqlite3
import warnings
from datetime import datetime, date
import requests as rq
import pandas as pd
from selenium import webdriver
from selenium.common.exceptions import WebDriverException
from dateutil.parser import parse
from settings import (
    AUTH,
    COLS,
    COLUMNS,
    USER_AGENT,
    URLS,
    ORG,
    CHROME_DRIVER
)


# logging
LEVEL = logging.INFO
LOG_FORMAT = "%(asctime)s - %(levelname)s - %(message)s"
HANDLERS = [logging.FileHandler('get.log'), logging.StreamHandler()]
logging.basicConfig(level=LEVEL, format=LOG_FORMAT, handlers=HANDLERS)
warnings.filterwarnings('ignore')

options = webdriver.ChromeOptions()
options.add_argument('headless')
options.add_argument('window-size=800x600')
# options.add_argument('--proxy-server=45.57.193.95:1337')
options.add_argument('--user-agent=' + USER_AGENT)


def get_site_cookies():
    ''' Retrieve login session and extract cookies.

        Go to expireddomains.net, login and collection
        session details. Cookies will be used to download
        csv files using requests.

    '''
    driver = webdriver.Chrome(CHROME_DRIVER, chrome_options=options)

    try:
        driver.get(AUTH['url'])
    except WebDriverException:
        return logging.exception()
        driver.quit()

    try:
        username = driver.find_element_by_xpath('//*[@id="inputLogin"]')
        username.clear()
        username.send_keys(AUTH['user'])
    except WebDriverException:
        return logging.exception('Login error')
        driver.quit()

    try:
        password = driver.find_element_by_xpath('//*[@id="inputPassword"]')
        password.clear()
        password.send_keys(AUTH['pass'])
    except WebDriverException:
        return logging.exception('Login error')
        driver.quit()

    try:
        button = (driver.find_element_by_xpath('//*[@id="content"]/div/div/div' +
                                               '/div[2]/form/div[4]/div/button'))
        button.click()
        logging.info('Logging in')
    except WebDriverException:
        return logging.exception('Login error')
        driver.quit()

    try:
        user_session = driver.get_cookies()
        logging.info('Cookies found')
    except WebDriverException:
        return logging.exception('Cookie error')
        driver.quit()

    # close browser window
    driver.quit()

    cookies = (list((filter(lambda cookie: cookie['name'] ==
                            'ExpiredDomainssessid', user_session))))

    return {cookies[0]['name']: cookies[0]['value']}

site_cookies = get_site_cookies()


def get_domain_data(url):
    '''
    Download each url in URLS.

    '''

    session = rq.session()
    session.cookies.update(site_cookies)

    try:
        _data = session.get(url, headers={'user-agent': USER_AGENT}, stream=True)
    except BaseException as e:
        logging.exception(e)

    data = _data.text.splitlines()
    logging.info('Downloaded {}, succesfully'.format(url))
    return data

domain_data = list((map(get_domain_data, URLS)))


def format_domain_data(data):
    '''
    Format and filter csv files.
    See Python pandas for clarification.

    '''

    data_to_csv = [ast.literal_eval(row.replace(';', ',')) for row in data]
    _domains = pd.DataFrame(data_to_csv, columns=data_to_csv[0])
    domains = _domains.drop(0).rename(columns=COLUMNS)
    domains['archive'] = ([int(date_[0:4]) for date_ in domains.archive])
    domains['whois'] = [int(date_[0:4]) for date_ in domains.whois]
    domains['dateadded'] = date.today()
    domains['fav'] = 0

    domains['age'] = ([datetime.today().year - date_
                       for date_ in domains.whois])

    domains['enddate'] = ([parse(endtime).strftime("%Y-%m-%d")
                           for endtime in domains.endtime])

    domains['io'] = ([{'registered': 1, 'available': 0}.get(status)
                      for status in domains.io])

    return domains

formatted_data = list(map(format_domain_data, domain_data))


def remove_domains_with_bids(domain):
    '''
   Not interested in domains with bids.

   '''

    if 'bids' in domain.columns:
        return domain[domain.bids == '0']
    elif 'bids' not in domain.columns:
        domain['bids'] = 0
        return domain

domains_no_bids = list((map(remove_domains_with_bids, formatted_data)))


rawgd, rawnj, rawsn, rawdd = map(lambda x: x[COLS], domains_no_bids)


rawgd['venue'] = 'gd'
rawgd['link'] = [re.sub('host', i, ORG['gdurl']) for i in rawgd.domains.tolist()]
rawnj['venue'] = 'nj'
rawnj['link'] = [re.sub('host', i, ORG['njurl']) for i in rawnj.domains.tolist()]
rawsn['venue'] = 'sn'
rawsn['link'] = [re.sub('host', i, ORG['snurl']) for i in rawsn.domains.tolist()]
rawdd['venue'] = 'dd'
rawdd['link'] = [re.sub('host', i, ORG['ddurl']) for i in rawdd.domains.tolist()]


_new_domains = pd.concat([rawnj, rawgd, rawsn, rawdd], ignore_index=True)
new_domains = _new_domains.drop_duplicates('domains')
