def multi_thread(fn, data, cores=4):
    '''
    :fn: Function
    :data: iterable
    :cores: nos of cores to use, default = 4
    '''
    pool = ThreadPool(cores)
    data = pool.map(fn, data)
    pool.close()
    pool.join()
    return data
