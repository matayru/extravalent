import re
import logging
import warnings
from functools import reduce
from datetime import datetime
from app.utilities import multi_thread
import enchant
import pandas as pd
from wordsegment import segment, load
import requests as rq
from scraper.misc import pnames, gnames, vwords
from scraper.constants import ORG, COLS_MAIN
from nltk import pos_tag, word_tokenize as tokenize
from search.searchapp.models.models import exp

# logging
LEVEL = logging.INFO
LOG_FORMAT = "%(asctime)s - %(levelname)s - %(message)s"
HANDLERS = [logging.FileHandler('sort.log'), logging.StreamHandler()]
logging.basicConfig(level=LEVEL, format=LOG_FORMAT, handlers=HANDLERS)


# settings
pd.set_option('mode.chained_assignment', None)
d = enchant.Dict("en_US")
load()


# retrieve formated data.
con =
raw_data = pd.read_sql('select * from raw_data limit 500;', con)


def name_place(item):
    '''
        Check for if name or place.
    '''
    if re.search('[a|e|i|o|u]', item):
        return {
            'is_name': int(bool(item in pnames.names)),
            'is_place': int(bool(item in gnames.names)),
            'host': item
        }


_new_domains = multi_thread(name_place, raw_data.host)
new_domains = pd.merge(raw_data, pd.DataFrame(_new_domains), how='inner')


def _split_host(host):
    return tuple(segment(host))

split_host = multi_thread(_split_host, new_domains.host)


new_domains['seg'] = split_host
new_domains['slen'] = [len(host) for host in new_domains.seg]

# divide data into 1,2, and 3 or more word groups
_two_word = new_domains[new_domains.slen == 2]
_one_word = new_domains[new_domains.slen == 1]
_three_word = new_domains[new_domains.slen > 2]

# add links for hoovers and findthecompany.com
_one_word['hov'] = [re.sub('host', i, ORG['hov']) for i in _one_word.host.tolist()]
_one_word['ftc'] = [re.sub('host', i, ORG['ftc']) for i in _one_word.host.tolist()]


def get_pos_category(seg_domain):
    '''
        groups pos tags into category '1' and '0' .
        1 category contains major postags and 0 contains all others
        see http://archive.is/zj2EH
        seg_domain is a tuple, you can see it on line 56
        using nltk to get pos_tags

    '''
    left, right = seg_domain
    postag = pos_tag(tokenize(left))[0][1] + pos_tag(tokenize(right))[0][1]

    if postag in ['JJNN', 'NNNN', 'NNNNS', 'JJNNS']:
        return {'host': left + right, 'md': 'Y', 'postag': postag}
    else:
        return {'host': left + right, 'md': 'N', 'postag': postag}


pos_category = pd.DataFrame(multi_thread(get_pos_category, _two_word.seg))


def valuable_words(seg_domain):
    '''
    checking for valuable words in modifier and head.
    Valuable words are words that have high frequency
    in domain sales.
    '''
    left, right = seg_domain
    return {'mod_head': str(left in vwords.mod)[0]
            + str(right in vwords.head)[0],
            'host': left + right}


val_words = pd.DataFrame(multi_thread(valuable_words, _two_word.seg))


def find_plurals(seg_domain):
    '''
    Used for searching for plural words in 2 word domains.
    Here we are identifying domains that end with 's'
    at the modifier or head.
    left is modifier, while right is head. Which is the same with
    first word been left and last word been right
    left_s means left ends with s, right s means right end with s.

    '''

    left, right = seg_domain

    if left.endswith('s') and right.endswith('s'):
        return {'left_s': 'Y', 'right_s': 'Y', 'host': left + right}
    if not left.endswith('s') and right.endswith('s'):
        return {'left_s': 'N', 'right_s': 'Y', 'host': left + right}
    if left.endswith('s') and not right.endswith('s'):
        return {'left_s': 'Y', 'right_s': 'N', 'host': left + right}
    if not left.endswith('s') and not right.endswith('s'):
        return {'left_s': 'N', 'right_s': 'N', 'host': left + right}


plurals = pd.DataFrame(multi_thread(find_plurals, _two_word.seg))


def get_search_volume(seg_domain):
    '''
    Is this a valid word combination?
    Valid combinations like "destroy property" or "red water"
    Invalid combinations like "destroy propertyz" or "red whater"
    Yes, valid! then get check search metrics.
    No!, invalid! then move on.

    Uses grepwords API.

    '''

    left, right = seg_domain
    if d.check(left) + d.check(right) == 2:
        keyword = left + '+' + right

        try:
            res = rq.get(
                "http://api.grepwords.com/lookup?q="
                + keyword + "&apikey=******").json()

            return {'ams': res[0]['ams'],
                    'cpc': res[0]['cpc'],
                    'host': (left + right),
                    'isword': 'Y'}

        except BaseException:
            return {'ams': 0, 'cpc': 0, 'host': (left + right), 'is_word': 'Y'}
    else:
        return {'ams': 0, 'cpc': 0, 'host': (left + right), 'is_word': 'N'}


search_volume = pd.DataFrame(multi_thread(get_search_volume, _two_word.seg))


# two word merge
frames = [_two_word, search_volume, val_words, pos_category, plurals]
two_word = pd.DataFrame(reduce(lambda x, y: pd.merge(x, y, on='host'), frames))

# merge 1,2,3 words
frames = [_one_word, two_word, _three_word]
dbvals_ = pd.concat(frames)[COLS_MAIN].fillna(0)
dbvals = dbvals_.to_dict(orient='records')


with db.atomic():
    for i in dbvals:
        exp.insert(i).execute()
