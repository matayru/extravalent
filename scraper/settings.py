import os

BASE_DIR = os.path.join(os.path.abspath(os.path.curdir))

AUTH = {
    'user': 'valleyrock',
    'pass': 'netmatrx.com',
    'url': 'https://www.expireddomains.net/login/'
}

USER_AGENT = ('Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 ' +
              '(KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36')

URLS = [
    'https://is.gd/Bg1m0g',
    'https://is.gd/rWoOie',
    'https://is.gd/2blRbA',
    'https://is.gd/98JQvf'
]

COLUMNS = {
    'Domain': 'domain',
    'ACR': 'crawls',
    'Bids': 'bids',
    'Price': 'price',
    'WBY': 'whois',
    'ABY': 'archive',
    'TLDs Reg': 'tldcount',
    'Endtime': 'endtime',
    'End Date': 'endtime',
    'IO': 'io',
    'LE': 'length'
}

COLS = [
    'domain',
    'host',
    'whois',
    'archive',
    'age',
    'tldcount',
    'crawls',
    'bids',
    'price',
    'dateadded',
    'enddate',
    'fav',
    'io',
    'length'
]

ORG = {
    'gdurl': 'https://pk.auctions.godaddy.com/trpItemListing.aspx?domain=host',
    'njurl': 'http://www.namejet.com/Pages/Auctions/BackorderDetails.aspx?domainname=host',
    'snurl': 'https://www.snapnames.com/domain/host.action',
    'ddurl': 'https://www.dynadot.com/market/auction/host',
    'hov': 'http://www.hoovers.com/company-information/company-search.html?term=host',
    'hov': 'http://www.hoovers.com/company-information/company-search.html?term=host',
    'ftc': 'http://listings.findthecompany.com/?launch_filters=[{"field":"company_name","operator":"LIKE","value":"host"}]',
    'dlurl': 'https://www.dynadot.com/market/backorder/host'
}

COLS_MAIN = ['domain', 'whois', 'archive', 'age', 'tldcount', 'crawls', 'venue',
             'ams', 'cpc', 'mod_head', 'link', 'enddate', 'fav', 'host', 'io',
             'length', 'dateadded', 'is_word', 'is_name', 'is_place', 'slen',
             'postag', 'ftc', 'hov', 'md', 'left_s', 'right_s']
