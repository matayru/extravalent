import os

BASE_DIR = os.path.join(os.path.abspath(os.path.curdir))
TEMPLATE_DIRS = [os.path.join(BASE_DIR,  'search/common/views'),
                 os.path.join(BASE_DIR,  'search/searchapp/views'),
                 os.path.join(BASE_DIR,  'search/contactapp/views')]
STATIC_DIR = os.path.join(BASE_DIR,  'search/common/static')
