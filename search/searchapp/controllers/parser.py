from search.searchapp.models.models import exp
from search.searchapp.controllers.humanize import ending
import re


def parsequery(queries):
        '''parse values from search form.
        '''

        q = []

        for query in queries:
            a, b = query

            if (re.match('(^tldcount$)|(^age$)|(^crawl$)|(^ams$)|(^cpc$)|(^length$)|(^whois$)|(^archive$)', a)
                and len(re.findall('^\d+$', b))) == 1:
                q.append('exp.' + a + '==' + b)

            if (re.match('(^tldcount$)|(^age$)|(^crawl$)|(^ams$)|(^cpc$)|(^length$)|(^whois$)|(^archive$)', a)
                and len(re.findall('<|>', b))) == 1:
                q.append('exp.' + a + b)

            elif (re.match('(^tldcount$)|(^age$)|(^crawl$)|(^ams$)|(^cpc$)|(^length$)|(^whois$)|(^archive$)', a)
                and len(re.findall('<|>', b))) == 2:
                low_, high_ = eval(b.replace('<', ',').replace('>', ''))
                q.append('exp.' + a + '>' + str(low_)), q.append('exp.' + a + '<' + str(high_))

            if re.match('(^fav$)|(^is_name$)|(^is_place$)|(^is_word$)|(^io$)|(^md$)', a) and b != '1.0':
                q.append('exp.' + a + '==' + b)
            elif re.match('(^fav$)|(^is_name$)|(^is_place$)|(^is_word$)|(^io$)|(^md$)', a) and b == '0':
                q.append('exp.' + a + '==' + b)

            if re.match('(^domain$)', a) and b.startswith('%'):
                q.append('exp.host.startswith(' + '"' + b[1:] + '"' + ')')

            elif re.match('(^domain$)', a) and b.endswith('%'):
                q.append('exp.host.endswith(' + '"' + b[:-1] + '"' + ')')

            if re.match('(^enddate$)', a) and re.match('(^today$)|(^tomorrow$)|(^dayafter$)|(^yesterday$)', b):
                q.append('exp.enddate==' + '"' + str(ending(b)) + '"')
            else:
                pass

            if re.match('(^slen$)', a) and re.match('(1)|(2)|(more)', b):
                q.append('exp.' + a + '==' + b)

            if re.match('(^venue$)', a) and re.match('(dd)|(nj)|(sn)|(gd)|(dl)', b):
                q.append('exp.' + a + '==' + '"' + b + '"')
            else:
                pass
            if re.match('(^MH$)', a) and re.match('(FT)|(TF)|(FF)|(TT)', b):
                q.append('exp.' + a + '==' + '"' + b + '"')
            else:
                pass

            if re.match('(^left_s$)', a) and b == '1.0':
                q.append('exp.' + a + '==' + b)

            if re.match('(^right_s$)', a) and b == '1.0':
                q.append('exp.' + a + '==' + b)

            query = list(map(eval, q))

        return exp.select().where(*query)


def watchlist():
    q = exp.select().where(exp.fav == 1)
    return q

def home():
    q = exp.select().limit(50)
    return q
