
def searchform(q):

    domain = ('domain', q.get('domain'))
    ams = ('ams', q.get('ams'))
    cpc = ('cpc', q.get('cpc'))
    enddate = ('enddate', q.get('enddate'))
    venue = ('venue', q.get('venue'))
    slen = ('slen', q.get('slen'))
    whois = ('whois', q.get('whois'))
    archive = ('archive', q.get('archive'))
    tldcount = ('tldcount', q.get('tldcount'))
    is_word = ('is_word', q.get('is_word'))
    is_name = ('is_name', q.get('is_name'))
    is_place = ('is_place', q.get('is_place'))
    length = ('length', q.get('length'))
    io = ('io', q.get('io'))
    mod_head = ('mod_head', q.get('mod_head'))
    right_s = ('right_s', q.get('right_s'))
    left_s = ('left_s', q.get('left_s'))

    query_ = [domain, ams, cpc, enddate, venue, slen, whois, archive,
              tldcount, is_word, is_name, is_place, length, io, mod_head,
              right_s, left_s]

    query = list(filter(lambda x: len(str(x[1])) > 0, query_))

    return query
