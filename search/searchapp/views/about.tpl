% include('head.tpl')

<h3 class="ui dividing header">We are still in development!</h3>

<h5>This application is in beta. See the future list for upcoming features.</h5>

<p> This application is simple. It provides an alternative search strategy and filter for finding hidden domain names.</p>
<p>At the moment, you can filter through domains using several interesting methods. The  <span class="ui horizontal label">LS</span>and <span class="ui horizontal label">RS</span> filters are the newest addition.
We found that several acquired names were in plural form; this prompted us to build this filter.</p> Here are some domains that can
be uncovered using this filter.

<ul class="ul list">
    <li>asthmapumps.com, sold for $204, January 24, 2018</li>
    <li>guavajuices.com, sold for $500, January 17, 2018</li>
</ul>

<p>This filter must be combined with the <span class="ui horizontal label">Count</span> and <span class="ui horizontal label">Word</span> filters, the Count filter should be set to "Two"; this ensures that only two word domains are returned, the Word filter should be set to "1"; this
ensures that only valid two word domains are returned. If the <span class="ui horizontal label">Word</span> filter is not set to "1", searches will return with
two word domain names, valid and invalid.</p>

<p>Invalid in this case means "none dictionary words"; we have an extensive dictionary and as such can
identify a wide range of words if <span class="ui horizontal label">Word</span>is set to "1".
</p>
<div class="ui hidden divider"></div>

<h3 class="ui dividing header">Future</h3>
<ul class="ul list">
    <li>User account, for private watchlists</li>
    <li>Words trend monitor; opportuinities for new markets</li>
    <li>Droplists and dropped lists</li>
    <li>Notifications</li>
</ul>
</div>
% include('foot.tpl')
