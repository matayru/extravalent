% include('head.tpl')

<!-- Search Begins Here -->
<h3 class="ui dividing header">
{{page}}


</h3>
<form method=POST action="/search/" class="ui mini form {{display}}">

<p>
See help page for information on how to search.
</p>

    <div class="ui grid">
        <div class="row">
            <div class="four wide column">
            <div class="field">
            <label>Domain</label>
            <input type="text" name="domain" placeholder="wordlike">
            </div>
            </div>


            <div class="two wide column">
            <div class="field">
            <label>AMS</label>
            <input type="text" name="ams" placeholder="searches p/m">
            </div>
            </div>

            <div class="two wide column">
            <div class="field">
            <label>CPC</label>
            <input type="text" name="cpc" placeholder="avg cpc">
            </div>
            </div>

            <div class="two wide column">
            <div class="field">
            <label>Ending</label>
            <select name="enddate">
            <option value="">Enddate</option>
            <option value="today">Today</option>
            <option value="tomorrow">Tomorrow</option>
            <option value="dayafter">Day After</option>
            <option value="yesterday">Yesterday</option>
            </select>
            </div>
            </div>

            <div class="two wide column">
            <div class="field">
            <label>Venue</label>
            <select name="venue">
            <option value="">Venue</option>
            <option value="nj">NJ</option>
            <option value="gd">GD</option>
            <option value="sn">SN</option>
            <option value="dd">DD</option>
            <option value="dl">DL</option>
            </select>
            </div>
            </div>

            <div class="two wide column">
            <div class="field">
            <label>Count</label>
            <select name="slen">
            <option value="">Count</option>
            <option value="1">One</option>
            <option value="2">Two</option>
            <option value="3">More</option>
            </select>
            </div>
            </div>

        </div>
            <div class="two wide column">
            <div class="field">
            <label>Whois</label>
            <input type="text" name="whois" placeholder="created">
            </div>
            </div>

            <div class="two wide column">
            <div class="field">
            <label>Archive</label>
            <input type="text" name="archive" placeholder="archive">
            </div>
            </div>

            <div class="one wide column">
            <div class="field">
            <label>Tld</label>
            <input type="text" name="tldcount" placeholder="">
            </div>
            </div>

            <div class="one wide column">
            <div class="field">
            <label>Word</label>
            <input type="text" name="is_word" placeholder="">
            </div>
            </div>

            <div class="one wide column">
            <div class="field">
            <label>Name</label>
            <input type="text" name="is_name" placeholder="">
            </div>
            </div>

            <div class="one wide column">
            <div class="field">
            <label>Place</label>
            <input type="text" name="is_place" placeholder="">
            </div>
            </div>

            <div class="one wide column">
            <div class="field">
            <label>Length</label>
            <input type="text" name="length" placeholder="">
            </div>
            </div>

            <div class="one wide column">
            <div class="field">
            <label>IO</label>
            <input type="text" name="io" placeholder="">
            </div>
            </div>


            <div class="one wide column">
            <div class="field">
            <label>MH</label>
            <input type="text" name="mod_head" placeholder="">
            </div>
            </div>


            <div class="one wide column">
            <div class="field">
            <label>LS</label>
            <input type="text" name="left_s" placeholder="">
            </div>
            </div>

            <div class="one wide column">
            <div class="field">
            <label>RS</label>
            <input type="text" name="right_s" placeholder="">
            </div>
            </div>

    </div>

<div class="ui hidden divider"></div>
<button class="ui button" >Submit</button>
</form>
<!-- Search Ends Here-->
<div class="ui hidden divider"></div>
<!-- Table Begins Here -->
<div ng-app="fav" ng-controller="saveFave" id="fav">
<table id="expired" class="ui celled striped very compact table">
    <thead>
        <tr>
        <th></th>
        <th>Domain</th>
        <th>Whois</th>
        <th>Archive</th>
        <th>Enddate</th>
        <th>CPC</th>
        <th>AMS</th>
        <th>Tld</th>
        <th>Crawls</th>
        <th>Iword</th>
        <th>MH</th>
        <th>MD</th>
        <th>FTC</th>
        <th>HOV</th>
        </tr>
    </thead>
    <tbody>
        %for row in rows:
        <tr>
        <td class="out">
        <a class="iconcolor" ng-click="dname(name='{{row.domain}}')" href="">
        <i ng-class="{{row.fav}}==1 ? 'show':'hide'" class="star icon outer isfull {{row.host}}"></i>
        <i class="empty star icon isempty"></i>
        </a>
        </td>
        <td><a target="_blank" href="{{row.link}}">{{row.domain}}</a></td>
        <td>{{row.whois}}</td>
        <td>{{row.archive}}</td>
        <td>{{row.enddate}}</td>
        <td>{{row.cpc}}</td>
        <td>{{row.ams}}</td>
        <td>{{row.tldcount}}</td>
        <td>{{row.crawls}}</td>
        <td>{{row.is_word}}</td>
        <td>{{row.mod_head}}</td>
        <td>{{row.md}}</td>
        <td><a target="_blank" href="{{row.ftc}}"><i class="arrow right icon"></i></a></td>
        <td><a target="_blank" href="{{row.hov}}"><i class="arrow right icon"></i></a></td>
        </tr>
        %end
    </tbody>

</table>

</div>
<!-- Table Ends Here -->
<div class="ui hidden divider"></div>
</div>
<!-- Main Container Ends Here-->

<!-- Footer Begins Here; see foot.tpl-->
% include('foot.tpl')
