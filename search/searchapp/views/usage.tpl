% include('head.tpl')

<!-- Search Begins Here -->
<h3 class="ui dividing header">Usage</h3>

<ul class="ui list">
<li>Use % as a prefix or suffix to find domains beginning or ending with a word.</li>
<li>Use > or < for AMS, CPC, Created, Archive, Tld and Length.
    <ul>
        <li>You can search in a range like this >200<1000</li>
        <li>You can use decimals for searching CPC, like >0.34</li>
        <li>Do not add spaces</li>
    </ul>
</li>
<li>For Word, Name, Place, IO, LS, RS - valid entries are only 1 or 0. 1 for yes and 0 for no.</li>
<li>For MH, valid entries are TT, FT, TF and FF</li>
</ul>

<h4>What do the filters do?</h4>
<table class="ui celled table">
    <thead>
        <tr>
        <th>Filter</th>
        <th>Description</th>
      </tr>
    </thead>
    <tbody>
            <tr>
              <td><span class="ui horizontal label">LS</span>
                <span class="ui horizontal label">RS</span></td>

              <td><p>The "S" in each of these filters stands for plural.
                 In English language, plurals are identified by adding a "S". These filters follow the same rule.<br>
                  LS identifies two word domains, where the first word is a plural. RS identifies two word domains where the second word in two word<br>
                  domain is a plural.</p>
              </td>

            </tr>
                <tr>
                    <td><span class="ui horizontal label">AMS</span>
                        <span class="ui horizontal label">CPC</span>
                    </td>
                    <td><p>AMS means average monthly searches</p>
                        <p>CPC means cost per click</p>
                </td>

            </tr>
            <tr>
                    <td><span class="ui horizontal label">TF</span>
                        <span class="ui horizontal label">FT</span>
                    </td>
                    <td><p>MH stands for modifier, head. T stand for true, and F stands for false.</p><p>In brief, in a two word domain; the first word is the modifier
                         and the second word is the head. This filter is unique because it <br> combines popular words found in domain name analysis
                        with cognitive theories.</p> <p><a href="https://is.gd/HGMiwT">Creating powerful brand names</a> for details on modifier and head</p>

                </td>

            </tr>
    </tbody>
</table>


<div class="ui hidden divider"></div>
</div>
% include('foot.tpl')
