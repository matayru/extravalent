from bottle import (error, response, template,
                    static_file, request, Bottle)
from search.searchapp.controllers.forms import searchform
from search.searchapp.controllers.bookmark import add_favorites
from config.common import STATIC_DIR
from search.searchapp.controllers.parser import parsequery, watchlist, home


app = Bottle()


@app.route('/')
def view_home():
    rows = home()
    return template('searchpage', rows=rows, page='Search', display='')


@app.route('/search/', method='POST')
def search():
    query = searchform(request.forms)
    rows = parsequery(query)
    return template('searchpage', rows=rows, page='', display='')


@app.route('/watchlist/')
def view_watchlist():
    rows = watchlist()
    return template('searchpage', rows=rows, page='Watchlist', display='hideelement')


@app.route('/usage/')
def usage():
    return template('usage')


@app.route('/about/')
def about():
    return template('about')


@app.route('/fav/<domain:re:[a-z]+\.com>', method='GET')
def add_favorite(domain):
    response.headers['Content-Type'] = 'application/json'
    response.headers['Access-Control-Allow-Methods'] = 'GET'
    return add_favorites(domain)


@app.route('/public/<filepath:path>')
def get_file(filepath):
    return static_file(filepath, root=STATIC_DIR)


@error(404)
@error(500)
def error404(error):
    return ('Your lost. Here try this '
            '<a href="/">Return home</a>')
