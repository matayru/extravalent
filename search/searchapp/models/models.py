import os
from peewee import *
from playhouse.db_url import connect, parse


class BaseModel(Model):
    class Meta:
        database = connect(os.environ.get('db_url'))

class exp(BaseModel):
    age = IntegerField()
    ams = IntegerField()
    whois = IntegerField()
    archive = IntegerField()
    cpc = DecimalField()
    crawls = IntegerField()
    dateadded = DateField()
    domain = CharField()
    enddate = DateField()
    fav = BooleanField()
    ftc = TextField()
    host = TextField()
    hov = TextField()
    io = BooleanField()
    is_word = BooleanField()
    length = FixedCharField()
    link = TextField()
    mod_head = TextField()
    is_name = BooleanField()
    is_place = BooleanField()
    postag = TextField()
    slen = IntegerField()
    tldcount = IntegerField()
    venue = FixedCharField()
    md = TextField()
    left_s = BooleanField()
    right_s = BooleanField()
