from bottle import route, template, run, Bottle
import os
from playhouse.db_url import connect, parse

app = Bottle()

@app.route('/')
def home():
    return os.environ.get('db_url')
