<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.13/semantic.min.css" />
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.semanticui.min.css" />


<style>
.hideelement{display:none;}
.out{position:relative;display:block;}
.isempty{position:relative;  }
.isfull{position:absolute; visibility:hidden}
.show{visibility:visible;}
.hide{visibility:hidden;}
#a.iconcolor:visited{color:#a8b2b9 !important;}
#a.iconcolor:link{color:#a8b2b9 !important;}
.sticky {
display: none;
}
</style>

</head>

<body>

<!-- Main Container Begins Here -->
<div class="ui container">

<!-- Main Navigation Begins Here -->
<div class="ui container menu">
  <div class="header item">
  Search Domains
</div>
    <a href="/" class="item">Home</a>
    <a href="/about/" class="item">About</a>
    <a href="/usage/" class="item">Help</a>
    <a href="/watchlist/" class="item">Watchlist</a>
</div>
<!-- Main Navigation Ends Here -->
