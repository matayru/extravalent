var tfConfig = {
        base_path: 'https://unpkg.com/tablefilter@0.5.40/dist/tablefilter/',
        alternate_rows: true,
        btn_reset: true,
        rows_counter: true,
        loader: true,
        status_bar: true,
        mark_active_columns: true,
        highlight_keywords: true,
        no_results_message: true,
        paging: true,
        
        col_0: 'none',
        col_1: '',
        col_2: '',
        col_3: '',
        col_4: '',
        col_5: '',
        col_6: '',
        col_7: '',
        col_8: '',
        col_9: '',
        col_10: '',
        col_11: '',
        col_12: '',


        
        col_types: [
            'string','string', 'number', 'number',
            'date', 'number', 'number','string',
            'number', 'number', 'string',
            'string', 'number', 
        ],
        col_widths: [
            '15px','100px', '80px', '80px',
            '150px', '80px', '80px',
            '80px', '80px', '80px',
            '80px', '80px', '80px',
        ],
        extensions: [{
            name: 'sort'
        }],

        /** Bootstrap integration */
        paging: {
          results_per_page: ['Records: ', [50, 100]]
        },

        // aligns filter at cell bottom when Bootstrap is enabled
        filters_cell_tag: 'th',

        // allows Bootstrap table styling
        themes: [{
            name: 'transparent'
        }]
    };
